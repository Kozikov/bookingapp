import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";


import { LogInComponent } from './log-in/log-in.component';
import { AuthComponent } from './auth.component';
import { NgModule } from '@angular/core';
import { AuthRoutingModule } from '../auth/auth-routing.module'

@NgModule({
    declarations: [
        LogInComponent,
        AuthComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        AuthRoutingModule
    ]
})

export class AuthModule {}