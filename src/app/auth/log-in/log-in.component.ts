import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegistrationService } from '../../shared/registration.service';
import { Admin } from '../../shared/interface';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.sass']
})
export class LogInComponent implements OnInit {

  name:string = 'cms_edu_admin@gmail.com';
  password:string = 'cms_edu_admin';
  registration:boolean = false;
  error = {
    text: '',
    value: false
  };

  admin: Admin;

  constructor(private logIn: RegistrationService) { }

  ngOnInit() {
  }

  submitForm(form: NgForm) {
    
    this.name = form.value.loginFormName;
    this.password = form.value.loginFormPassword;
    let data = {
      email: this.name,
      password: this.password
    }

    this.logIn.logIn(data)
    .subscribe(
      (data) => {
        console.log(data)
      },
      (err) => {
        this.error.text = err.error.message;
        this.error.value = true;
        
      }
      );

  }

}
