export interface Admin {
    Item: {
        email: string,
        fullName: string,
        id: string,
        project: string,
        disabledBooking: boolean,
        refresh_token: string,
        created_at: number,
    },
    access_token: string,
    expires_in: number
}
