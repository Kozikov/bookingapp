import { Injectable } from '@angular/core';
import { map, retry, catchError, filter } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Admin } from './interface';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  url: string = 'https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0';
  urlSignIn: string = '/auth/signin';
  

 
  constructor(private http: HttpClient) { }

  logIn(data): Observable<Admin>{
    return this.http.post(`${this.url}/auth/signin`, data)
    .pipe(
      map((admin: Admin) => admin)
    )
  }

}
